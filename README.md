![alt tag](http://s8.postimg.org/g8pv2s9p1/118063_2198095_194747_image.jpg)

[![Join the chat at https://gitter.im/MCrafterss/Op-Shield](https://badges.gitter.im/MCrafterss/Op-Shield.svg)](https://gitter.im/MCrafterss/Op-Shield?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge)



|   Developers  |         More Information            |
|---------------|-------------------------------------|
| Amir Emad |     Main Developer of The Plugin       |
| PocketKiller|    Fixed Some Syntax Errors     |

Op-Manager is used to disable commands for ops and protect them from bieng kicked or banned

Op-Manager will always be updated with all mcpe versions :smile:

#Dependency : PurePerms

#Permissions 

note all perms are set to false so setting them to a player will run the function on that player only:

|      Permission                   |             Usage             |
|-----------------------------------|-------------------------------|
|opmanager.nokick                |Protects op from being kicked.   |
| | |
|opmanager.noban                 |Protects op from being banned.   |
| | |
|opmanager.disablestop.command   |Disables stop command.          |
| | |
|opmanager.disabletime.command   |Disables time command.         |
| | |
|opmanager.disablewhitelist.command   |Disables whitelist command.        |
| | |
|opmanager.disablekill.command   |Disables kill command.          |
| | |
|opmanager.disable.command   |Disables stop command.        |
| | |
|opmanager.disablesay.command   |Disables say command.          |
| | |
|opmanager.disableban.command   |Disables ban command.          |
| | |
|opmanager.disablebanip.command   |Disables ban-ip command.          |
| | |
|opmanager.disabletp.command   |Disables tp command.         |
| | |
|opmanager.disabledeop.command   |Disables deop command.         |
| | |
|opmanager.disableop.command   |Disables op command.         |
| | |
|opmanager.disableme.command  |Disables me command.        |


