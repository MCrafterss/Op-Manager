<?php

/*
 *
 *   ____        __  __                                   
 *  / __ \      |  \/  |                                  
 * | |  | |_ __ | \  / | __ _ _ __   __ _  __ _  ___ _ __ 
 * | |  | | '_ \| |\/| |/ _` | '_ \ / _` |/ _` |/ _ \ '__|
 * | |__| | |_) | |  | | (_| | | | | (_| | (_| |  __/ |   
 *  \____/| .__/|_|  |_|\__,_|_| |_|\__,_|\__, |\___|_|   
 *        | |                              __/ |          
 *        |_|                             |___/           
 *
 * OpManager is Soon Number #1 Op Managment Plugin
 *
 *
 *
*/

namespace mcrafters;
 
use pocketmine\event\player\PlayerKickEvent;
use pocketmine\utils\Config;
use pocketmine\event\Listener;
use pocketmine\plugin\PluginBase;
use pocketmine\utils\TextFormat as MT;
use pocketmine\event\player\PlayerPreLoginEvent;
use pocketmine\event\player\PlayerChatEvent;
use pocketmine\event\player\PlayerCommandPreprocessEvent;

class OpManager extends PluginBase implements Listener
{
	public $cfg;
	
      public function onEnable()
    {
    	$cfg = $this->getConfig();
        $this->getServer()->getPluginManager()->registerEvents($this, $this);
        $this->getServer()->getLogger()->info(MT::AQUA . "Op-" . MT::YELLOW . "Manager" . MT::GREEN . " has loaded");
        $this->saveResource("config.yml");
        $cfg = new Config($this->getDataFolder() . "config.yml", Config::YAML);
        }
      // opcantbekicked
      public function onKick(PlayerKickEvent $event)
      {
        if($event->getPlayer()->hasPermission('opmanager.nokick')){
			$event->setCancelled(true);
   }
} 
      // opcantbebanned
      public function onPreLogin(PlayerPreLoginEvent $event)
      {
        if($event->getPlayer()->isBanned() and $event->getPlayer()->hasPermission('opmanager.noban')){$event->getPlayer()->setBanned(false);
    }
	  }
      // disable stop command
      public function onCommandPreprocess(PlayerCommandPreprocessEvent $event)
      {
      	$cfg = $this->getConfig();
        if($event->getMessage() === "/stop" and $event->getPlayer()->hasPermission('opmanager.disablestop.command')){
            $event->getPlayer()->sendMessage($this->getConfig()->get("Disable-Stop-Command"));
            $event->setCancelled();
        }
                if($event->getMessage() === "/say" and $event->getPlayer()->hasPermission('opmanager.disablesay.command')){
            $event->getPlayer()->sendMessage($this->getConfig()->get("Disable-Say-Command"));
            $event->setCancelled();
				}
                    if($event->getMessage() === "/ban" and $event->getPlayer()->hasPermission('opmanager.disableban.command')){
            $event->getPlayer()->sendMessage($this->getConfig()->get("Disable-ban-Command"));
            $event->setCancelled();
					}
                    if($event->getMessage() === "/ban-ip" and $event->getPlayer()->hasPermission('opmanager.disablebanip.command')){
            $event->getPlayer()->sendMessage($this->getConfig()->get("Disable-banip-Command"));
            $event->setCancelled();
					}
                    if($event->getMessage() === "/whitelist" and $event->getPlayer()->hasPermission('opmanager.disablewhitelist.command')){
            $event->getPlayer()->sendMessage($this->getConfig()->get("Disable-whitelist-Command"));
            $event->setCancelled();
					}
                    if($event->getMessage() === "/kill" and $event->getPlayer()->hasPermission('opmanager.disablekill.command')){
            $event->getPlayer()->sendMessage($this->getConfig()->get("Disable-kill-Command"));
            $event->setCancelled();
					}
                    if($event->getMessage() === "/time" and $event->getPlayer()->hasPermission('opmanager.disabletime.command')){
            $event->getPlayer()->sendMessage($this->getConfig()->get("Disable-time-Command"));
            $event->setCancelled();
					}
                    if($event->getMessage() === "/op" and $event->getPlayer()->hasPermission('opmanager.disableop.command')){
            $event->getPlayer()->sendMessage($this->getConfig()->get("Disable-op-Command"));
            $event->setCancelled();
					}
                    if($event->getMessage() === "/deop" and $event->getPlayer()->hasPermission('opmanager.disabledeop.command')){
            $event->getPlayer()->sendMessage($this->getConfig()->get("Disable-deop-Command"));
            $event->setCancelled();
					}
                    if($event->getMessage() === "/tp" and $event->getPlayer()->hasPermission('opmanager.disabletp.command')){
            $event->getPlayer()->sendMessage($this->getConfig()->get("Disable-tp-Command"));
            $event->setCancelled();
                    }
                   if($event->getMessage() === "/me" and $event->getPlayer()->hasPermission('opmanager.disableme.command')){
            $event->getPlayer()->sendMessage($this->getConfig()->get("Disable-me-Command"));
            $event->setCancelled();
                    }
}
public function translateColors($symbol, $message){
	
	        $message = str_replace("&", "§", $message);
		$message = str_replace($symbol."0", MT::BLACK, $message);
		$message = str_replace($symbol."1", MT::DARK_BLUE, $message);
		$message = str_replace($symbol."2", MT::DARK_GREEN, $message);
		$message = str_replace($symbol."3", MT::DARK_AQUA, $message);
		$message = str_replace($symbol."4", MT::DARK_RED, $message);
		$message = str_replace($symbol."5", MT::DARK_PURPLE, $message);
		$message = str_replace($symbol."6", MT::GOLD, $message);
		$message = str_replace($symbol."7", MT::GRAY, $message);
		$message = str_replace($symbol."8", MT::DARK_GRAY, $message);
		$message = str_replace($symbol."9", MT::BLUE, $message);
		$message = str_replace($symbol."a", MT::GREEN, $message);
		$message = str_replace($symbol."b", MT::AQUA, $message);
		$message = str_replace($symbol."c", MT::RED, $message);
		$message = str_replace($symbol."d", MT::LIGHT_PURPLE, $message);
		$message = str_replace($symbol."e", MT::YELLOW, $message);
		$message = str_replace($symbol."f", MT::WHITE, $message);
		$message = str_replace($symbol."l", MT::BOLD, $message);
		$message = str_replace($symbol."o", MT::ITALIC, $message);
		$message = str_replace($symbol."r", MT::RESET, $message);
	
		return $message;
	}
public function onDisable()
{
$this->getLogger()->info(MT::AQUA . "Op-" . MT::YELLOW . "Manager" . MT::RED . " has unloaded");
   }
}		
?>
